const convert = require('xml-js');
const detectIndent = require('detect-indent')

module.exports.readVersion = function (contents) {
    const pom = convert.xml2json(contents, {compact: true});
    return JSON.parse(pom).project.version["_text"];
}

module.exports.writeVersion = function (contents, version) {
    const pom = convert.xml2js(contents, {compact: true});
    const indent = detectIndent(contents).indent || '    ';
    return convert.js2xml({
        ...pom,
        project: {
            ...pom.project,
            version: {
                ...pom.project.version,
                "_text": version
            }
        }
    }, {compact: true, spaces: indent});
}
