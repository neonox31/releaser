FROM node:lts-alpine

RUN apk add --no-cache git gnupg

COPY package.json /releaser/package.json
COPY updaters /releaser/updaters
RUN yarn --cwd /releaser

WORKDIR /workspace

ENTRYPOINT ["/releaser/node_modules/standard-version/bin/cli.js"]
