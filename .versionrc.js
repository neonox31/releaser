module.exports = {
    tagPrefix: "",
    releaseCommitMessageFormat: "chore(release): release version {{currentTag}}",
    packageFiles: [
        {
            filename: "package.json",
            type: "json"
        }
    ],
    bumpFiles: [
        {
            filename: "package.json",
            type: "json"
        }
    ]
};
