# Neonox31 releaser

This releaser is based on https://github.com/conventional-changelog/standard-version

Every parameter or configuration are identical to `standard-version` project.

## Usage

### Npx

```bash
npx @neonox31/releaser --help
```

### Node (NPM or yarn)

```bash
yarn add -D @neonox31/releaser # install
yarn releaser --help # use
```

### Docker

```bash
docker run --rm -it \
   -v ~/.gitconfig:/etc/gitconfig \
   -v $(pwd):/workspace \
   neonox31/releaser --help
```

ℹ If you are using a GPG key for signing your commits, you must add some volumes to the container run command :
```bash
docker run --rm -it \
   -v $HOME/.gnupg/pubring.kbx:/root/.gnupg/pubring.kbx \
   -v $HOME/.gnupg/trustdb.gpg:/root/.gnupg/trustdb.gpg \
   -v $(gpgconf --list-dirs agent-extra-socket):/root/.gnupg/S.gpg-agent:z \
   -v ~/.gitconfig:/etc/gitconfig \
   -v $(pwd):/workspace \
   neonox31/releaser --help
```

## Examples

This releaser can also release non based nodejs projects using custom updaters, here are some examples here.
Updaters could be found under `releaser/updaters` with docker image and under `node_modules/@neonox31/releaser/updaters` with standard package installation.

### Maven projects

Define a `versionrc.js` file at the root of your project with following content :
```js
module.exports = {
  bumpFiles: [
    {
      "filename": "pom.xml",
      "updater": "/releaser/updaters/pom-updater.js"
    },
  ]
};
```
